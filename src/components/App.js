import './App.css';
import Description from './pages/Description';
import Header from './pages/Header';

import LandingPage from './pages/LandingPage';

function App() {
  return (
    <>
      <div className='bg-yellow-500'>
        <Header />
        <LandingPage />
      </div>
      <Description />
    </>
  );
}

export default App;
