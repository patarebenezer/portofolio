import gambar from '../../../assets/img/me.png'
import linkedin from '../../../assets/icons/linkedin-48.png'
import whatsapp from '../../../assets/icons/whatsapp-50.png'
import github from '../../../assets/icons/github-256.png'

const LandingPage = () => {
  return (
    <div className='flex flex-col lg:flex-row lg:justify-evenly items-center'>
      <div className=''>
        <h1 className='font-bold text-4xl'>Yo! I'm Patar, <br />Frontend Developer.</h1>
        <p className='font-light mt-4'>Let's collaborate on your next project.</p>
        <div className='button mt-12 flex gap-3'>
          <a href='mailto:patarebenezer05@gmail.com' className='w-auto bg-black text-white rounded-3xl px-8 py-2'>Let's work</a>
          <a href='https://drive.google.com/drive/folders/1zFg5QkB9RIs6pcQFCUrRBIU7K_eGKaVi?usp=sharing' target={"_blank"} className='w-auto border-2 border-black font-medium rounded-3xl px-8 py-2'>See My CV</a>
        </div>
        <div className='logo mt-16 flex gap-6 items-center'>
          <a href="https://gitlab.com/patarebenezer" target="_blank" rel="noreferrer">
            <img src={github} alt="" className='w-6'/>
          </a>
          <a href="https://www.linkedin.com/in/patar-siahaan-87a50318b/" target="_blank" rel="noreferrer">
            <img src={linkedin} alt="" className='w-5'/>
          </a>
          <a href="https://wa.me/08125055914" target="_blank" rel="noreferrer">
            <img src={whatsapp} alt="" className='w-5'/>
          </a>
        </div>
      </div>
      <div className=''>
        <img src={gambar} alt="" className='w-48 lg:w-96 lg:h-1/2 lg:px-9'/>
      </div>
    </div>
  )
}

export default LandingPage