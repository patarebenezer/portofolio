import tsel from '../../../assets/img/tsel.png'
import mikroskil from '../../../assets/img/mikroskil.png'
import codepolitan from '../../../assets/img/codepolitan.png'
import bwa from '../../../assets/img/bwa.png'
import binar from '../../../assets/img/binar.png'
import mbi from '../../../assets/img/mbi.png'

const Description = () => {
    return (
        <>
            <div className="mt-20 flex justify-evenly mb-16">
                <div className="w-10/12 lg:w-8/12">
                    <h1 className="text-4xl">Experience</h1>
                    <p className='text-gray-400'>Mar 2019 - Jun 2022</p>
                    <p className="text-gray-500 mt-3 mb-3">I've worked with telkomsel Medan as a Fullstack Web more then 3 years and get a lot of knowledge about data processing.</p>
                    <div className='flex flex-col lg:flex-row gap-1 lg:gap-12 lg:items-center w-full justify-start'>
                        <img src={tsel} alt="" className='w-48 h-auto' />
                        <img src={mbi} alt="" className='w-52 h-auto object-center' />
                    </div>
                </div>
            </div>

            <div className="mt-10 flex justify-evenly mb-16">
                <div className="w-10/12 lg:w-8/12">
                    <h1 className="text-4xl">Education</h1>
                    <p className='text-gray-400'>2014 - 2019</p>
                    <p className="text-gray-500 mt-3 mb-8">I completed my studies in Mikroskil with a major in Informatics Engineering</p>
                    <img src={mikroskil} alt="" className='w-48' />
                </div>
            </div>

            <div className="mt-10 flex justify-evenly mb-16">
                <div className="w-10/12 lg:w-8/12">
                    <h1 className="text-4xl">Online Bootcamp</h1>
                    <p className='text-gray-400'>2017 - 2022</p>
                    <p className="text-gray-500 mt-3 mb-8">I completed several bootcamps online learning and currently I am still studying at the Binar Academy about topic Frontend Engineer</p>
                    <div className='flex gap-5 items-center'>
                        <img src={codepolitan} alt="" className=' w-24 h-24' />
                        <img src={bwa} alt="" className='w-16 h-16' />
                        <img src={binar} alt="" className='w-30 h-20' />
                    </div>
                </div>
            </div>

            <div className="mt-10 flex justify-evenly mb-16">
                <div className="w-10/12 lg:w-8/12">
                    <h1 className="text-4xl">My Exercise </h1>
                    <p className='text-gray-400'>2022</p>
                    <p className="text-gray-500 mt-3 mb-8">Some time ago the exercise I did</p>
                    <div className='grid grid-cols-1 gap-3'>
                        <a target={'_blank'} className='hover:underline rounded-md p-1 hover:text-blue-500 text-blue-600' href="https://binar-car-rent.netlify.app/">https://binar-car-rent.netlify.app/</a>
                        <a target={'_blank'} className='hover:underline rounded-md p-1 hover:text-blue-500 text-blue-600' href="https://quiz-harisenin.netlify.app/">https://quiz-harisenin.netlify.app/</a>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Description
